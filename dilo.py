import os

from google.cloud import dialogflow_v2 as dialogflow

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]= "ga.json"

client  = dialogflow.SessionsClient()
session = client.session_path(project="alert-operation-339022" , session="me")
text_input = dialogflow.TextInput(text="hola", language_code = "es")
query_input = dialogflow.QueryInput(text=text_input)
response =  client.detect_intent(query_input=query_input , session=session)

print(response.query_result.fulfillment_text)